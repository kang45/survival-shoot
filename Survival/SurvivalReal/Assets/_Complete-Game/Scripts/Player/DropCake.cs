﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CompleteProject
{
    public class DropCake : MonoBehaviour
    {
        public GameObject player;
        public GameObject cake;
        public GameObject zomBunny;
        public GameObject zomBear;
        public GameObject Hellephant;
        public GameObject holdingCake;
        EnemyMovement enemymovement;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Drop();
            }

        }

        void Drop()
        {
            Vector3 cakeLocation = holdingCake.transform.position;
            GameObject cakeCopy = Instantiate(cake, cakeLocation, Quaternion.identity);
            StartCoroutine(ClearingCake(cakeCopy));

        }

        void SwapTag(GameObject cakeCopy)
        {
           // string tag1 = cakeCopy.tag;
           // string tag2 = player.tag;

           // player.tag = tag1;
           // cakeCopy.tag = tag2;

          //  enemymovement = zomBunny.GetComponent<EnemyMovement>();
          //  enemymovement.SwapPlayer(cakeCopy.transform);


        }

        IEnumerator ClearingCake(GameObject cakeCopy)
        {
            yield return new WaitForSeconds(5);
        //    enemymovement = zomBunny.GetComponent<EnemyMovement>();
         //   enemymovement.SwapPlayer(player.transform);
            Destroy(cakeCopy);
        }
    }
}